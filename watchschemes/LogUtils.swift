//
//  LogUtils.swift
//  watchlogging
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//

import Foundation

func ENTRY_LOG(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) -> Void {
  log.debug("ENTRY", functionName:functionName, fileName:fileName, lineNumber:lineNumber)
}

func EXIT_LOG(functionName:String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) -> Void {
  log.debug("EXIT", functionName:functionName, fileName:fileName, lineNumber:lineNumber)
}